import Vue from "vue";
import VueRouter from "vue-router";
// import loginUtil from "@/utils/loginUtil";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import {mainNotMenu, notMain} from "./routerBuilder";

Vue.use(VueRouter);

const routes = [
  //首页配置请放置在第一位
  mainNotMenu([{title: "主页", path: "/main", componentPath: "main"}]),
  // notMain("主页", "main", "main"),

  notMain("登录", "login", "login")
];

const router = new VueRouter({
  routes,
});

const whiteUris = ["/login","/main"]; // 白名单,无需登录即可访问

router.beforeEach((to, from, next) => {
  NProgress.start();
  if (whiteUris.indexOf(to.path) !== -1) {
    // 在白名单当中或者已经登录,放行
    next();
  } else {
    toLogin(to.fullPath);
  }
  // if (whiteUris.indexOf(to.path) !== -1 || loginUtil.isLogin()) {
  //   // 在白名单当中或者已经登录,放行
  //   next();
  // } else {
  //   toLogin(to.fullPath);
  // }
});

router.afterEach(() => {
  NProgress.done();
});

export const toLogin = () => {
  router.push({
    path: "/login",
  });
};

export {routes};

export default router;
